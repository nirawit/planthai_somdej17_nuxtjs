import jwt_decode from 'jwt-decode'
const token = sessionStorage.getItem('token')
import moment from 'moment'
let decoded = ''
if (token !== null) {
  decoded = jwt_decode(token)
}
export const state = () => ({
  auth: {
    status: moment(new Date(decoded.exp * 1000)) > moment() ? true : false,
    level: decoded.member_status_level || '',
    exp: decoded.exp || null,
  },
  line: {
    pictureUrl: null,
    displayName: null,
    userId: null,
  },
  user: {
    member_fullname: decoded.member_fullname || null,
    member_gender: decoded.member_gender || null,
    member_birthday: decoded.member_birthday || null,
    member_congenitaldisease: decoded.member_congenitaldisease || null,
    member_tel: decoded.member_tel || null,
    member_line_userId: decoded.line_userId || null,
    member_img: decoded.member_img || null,
  },
  // register: {
  //   fullname: '',
  //   gender: 1,
  //   phone: '',
  //   birthday: new Date().toISOString().substr(0, 10),
  // },
  // dialog: {
  //   isShow: false,
  //   title: '',
  //   message: '',
  // },
})

export const getters = {
  getAuth(state) {
    return state.auth
  },
  getUser(state) {
    return state.user
  },
  // getRegister(state) {
  //   return state.register
  // },
  // getDialog(state) {
  //   return state.dialog
  // },
  getLine(state) {
    return state.line
  },
}

export const mutations = {
  SET_AUTH(state, data) {
    state.auth = {
      ...state.auth,
      ...data,
    }
  },
  // SET_REGISTER(state, data) {
  //   state.register = {
  //     ...state.register,
  //     ...data,
  //   }
  // },
  // SET_DIALOG(state, data) {
  //   state.dialog = {
  //     ...state.dialog,
  //     ...data,
  //   }
  // },
  SET_USER(state, data) {
    state.user = {
      ...state.user,
      ...data,
    }
  },
  SET_LINE(state, data) {
    state.line = {
      ...state.line,
      ...data,
    }
  },
}

export const actions = {
  setAuth({ commit }, data) {
    commit('SET_AUTH', data)
  },
  setUser({ commit }, data) {
    commit('SET_USER', data)
  },
  // setRegister({ commit }, data) {
  //   commit('SET_REGISTER', data)
  // },
  // setDialog({ commit }, data) {
  //   commit('SET_DIALOG', data)
  // },
  setLine({ commit }, data) {
    commit('SET_LINE', data)
  },
}
