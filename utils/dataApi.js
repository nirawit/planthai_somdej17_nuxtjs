import { request } from './api'

const HOSNAME = process.env.apiBaseUrl
//========= Auth =============//
export function register(data) {
  const url = `${HOSNAME}/api/auth/register`
  return request('post', url, { data }, false)
}

export function checkliffAuth(data) {
  const url = `${HOSNAME}/api/auth/checkliff_first`
  return request('post', url, { data }, false)
}
//========= End Auth =============//

export function servicePlanthai() {
  const url = `${HOSNAME}/api/line_liff/service_planthai`
  return request('get', url, {}, true)
}

export function getDateDisable() {
  const url = `${HOSNAME}/api/line_liff/booking_dateyear_disable`
  return request('get', url, {}, true)
}

export function bookingLimit(id, date) {
  const url = `${HOSNAME}/api/line_liff/selectbooking_limit/${id}/${date}`
  return request('get', url, {}, true)
}

export function saveInOrderBooking(data) {
  const url = `${HOSNAME}/api/line_liff/save_booking`
  return request('post', url, { data }, true)
}

export function getOrderBooking() {
  const url = `${HOSNAME}/api/line_liff/get_inorderbooking`
  return request('get', url, {}, true)
}

export function getOrderBookingHistory() {
  const url = `${HOSNAME}/api/line_liff/get_inorderbooking_history`
  return request('get', url, {}, true)
}

export function getMyProfile() {
  const url = `${HOSNAME}/api/line_liff/profile`
  return request('get', url, {}, true)
}

export function updateMyProfile(data) {
  const url = `${HOSNAME}/api/line_liff/profile`
  return request('put', url, { data }, true)
}

export function logout() {
  const url = `${HOSNAME}/api/v1/user/sign_out`
  return request('delete', url, {}, true)
}

// export function create(data) {
//     const url = `${HOSNAME}/api/v1/covid19/save`
//     return request(
//         'post',
//         url, {
//             data
//         },
//         false
//     )
// }

export function update(id, title, body) {
  const url = `${HOSNAME}/api/v1/user/blogs/${id}`
  return request(
    'put',
    url,
    {
      blog: { title, body },
    },
    true
  )
}

export function del(id) {
  const url = `${HOSNAME}/api/v1/user/blogs/${id}`
  return request('delete', url, {}, true)
}
