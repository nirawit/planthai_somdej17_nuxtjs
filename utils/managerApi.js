import { request } from './api'

const HOSNAME = process.env.apiBaseUrl

export function getManagerQ() {
  const url = `${HOSNAME}/api/manager/get_manager_q`
  return request('get', url, {}, true)
}

export function approvedQ(data) {
  const url = `${HOSNAME}/api/manager/approved_q`
  return request('put', url, { data }, true)
}

export function alertQ(data) {
  const url = `${HOSNAME}/api/manager/alert_q`
  return request('post', url, { data }, true)
}

export function checkInQ(data) {
  const url = `${HOSNAME}/api/manager/checkin_q`
  return request('post', url, { data }, true)
}

export function serviceList() {
  const url = `${HOSNAME}/api/manager/service_list`
  return request('get', url, {}, true)
}

export function serviceListMore(id) {
  const url = `${HOSNAME}/api/manager/service_list_more/${id}`
  return request('get', url, {}, true)
}

export function getMemberList() {
  const url = `${HOSNAME}/api/manager/getmember_list`
  return request('get', url, {}, true)
}

export function updateMemberStatus(data) {
  const url = `${HOSNAME}/api/manager/update_memberstatus`
  return request('put', url, { data }, true)
}

export function getHolidayAll() {
  const url = `${HOSNAME}/api/manager/get_holiday`
  return request('get', url, {}, true)
}

export function saveHoliday(data) {
  const url = `${HOSNAME}/api/manager/save_holiday`
  return request('post', url, { data }, true)
}

export function delHoliday(data) {
  const url = `${HOSNAME}/api/manager/del_holiday`
  return request('delete', url, { data }, true)
}

export function getSpecialAll() {
  const url = `${HOSNAME}/api/manager/special_day`
  return request('get', url, {}, true)
}

export function saveSpecialDay(data) {
  const url = `${HOSNAME}/api/manager/special_day`
  return request('post', url, { data }, true)
}

export function delSpecialDay(data) {
  const url = `${HOSNAME}/api/manager/special_day`
  return request('delete', url, { data }, true)
}

export function saveService(data) {
  const url = `${HOSNAME}/api/manager/service_planthai`
  return request('post', url, { data }, true)
}
