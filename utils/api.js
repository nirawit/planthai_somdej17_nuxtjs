import axios from 'axios'

const getToken = function () {
  if (process.server) {
    // server side
    return
  }
  if (window.$nuxt) {
    return window.sessionStorage.getItem('token')
  }
}

export async function request(method, url, data, auth = false) {
  const headers = {}
  if (auth) {
    // headers['auth-token'] = getToken()
    headers['Authorization'] = `Bearer ${getToken()}`
  }
  try {
    // call api
    const response = await axios({
      method,
      url,
      data,
      headers,
    })

    if (response.data.code == 401) {
      console.log('response', response.data.message)
      window.location.href = '/'
    }
    return response
  } catch (e) {
    return e
  }
}
