module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
    extends: [
        // '@nuxtjs',
        // 'prettier',
        'prettier/vue',
        // 'plugin:prettier/recommended',
        // 'plugin:nuxt/recommended',
        'plugin:vue/essential',
        'eslint:recommended',
    ],
    plugins: ['prettier'],
    // add your custom rules here
    rules: {
        'nuxt/no-cjs-in-config': 'off',
    },
}

// module.exports = {
//     root: true,
//     env: {
//         browser: true,
//         node: true,
//     },
//     parserOptions: {
//         parser: 'babel-eslint',
//     },
//     extends: [
//         //'@nuxtjs',
//         //'plugin:nuxt/recommended',
//         //'plugin:vue/recommended',
//         'plugin:vue/essential',
//         'eslint:recommended',
//     ],
// }