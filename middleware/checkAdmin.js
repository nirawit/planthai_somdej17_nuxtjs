// const token = sessionStorage.getItem('token')
export default function ({ store, redirect }) {
  // let decoded = ''
  // if (token !== null) {
  //   jwt_decode(token)
  // }
  if (
    store.getters.getAuth.level === 'admin' ||
    store.getters.getAuth.level === 'superAdmin'
  ) {
    return
  } else {
    return redirect('/')
  }
}
