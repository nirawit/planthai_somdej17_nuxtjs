export default function ({ store, redirect }) {
  if (store.getters.getAuth.level !== 'member') {
    return redirect('/')
  }
}
